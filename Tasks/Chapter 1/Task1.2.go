// Echo program with outputing an index of passed argument with itself
package main

import (
	"fmt"
	"os"
)

func main() {
	for index, arg := range os.Args[1:] {
		fmt.Printf("%d: %v\n", index, arg)
	}
}
